package carlos.ramirez.pm.facci.recuperacion.rest.Adaptador;

import java.util.List;

import carlos.ramirez.pm.facci.recuperacion.rest.Constanst.ApiConstanst;
import carlos.ramirez.pm.facci.recuperacion.rest.Modelo.Materias;
import carlos.ramirez.pm.facci.recuperacion.rest.Service.MateriesServices;
import retrofit2.Call;

public class MateriasAdapter extends BaseAdapter implements MateriesServices{

    public MateriesServices materiesServices;

    public MateriasAdapter() {
        super( ApiConstanst.BASE_MATERIAS_URL);
        materiesServices = createService(MateriesServices.class);
    }

    @Override
    public retrofit2.Call <List <Materias>> getMaterias() {
        return materiesServices.getMaterias();
    }

    @Override
    public Call <Materias> getMateria(String id) {
        return materiesServices.getMateria(id);
    }
}
