package carlos.ramirez.pm.facci.recuperacion.rest.Modelo;

import com.google.gson.annotations.SerializedName;

public class Materias {

    @SerializedName("id")
    private String id;

    @SerializedName("descripcion")
    private String descripcion;

    @SerializedName("parcial_uno")
    private String parcial_uno;

    @SerializedName("parcial_dos")
    private String parcial_dos;

    @SerializedName("aprueba")
    private String aprueba;

    public String getId() {
        return id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getParcial_uno() {
        return parcial_uno;
    }

    public String getParcial_dos() {
        return parcial_dos;
    }

    public String getAprueba() {
        return aprueba;
    }
}
