package carlos.ramirez.pm.facci.recuperacion.rest.Service;



import java.util.List;

import carlos.ramirez.pm.facci.recuperacion.rest.Constanst.ApiConstanst;
import carlos.ramirez.pm.facci.recuperacion.rest.Modelo.Materias;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface MateriesServices {

    @GET(ApiConstanst.MATERIAS)
    Call<List<Materias>> getMaterias();

    @GET(ApiConstanst.MATERIA)
    Call<Materias> getMateria(@Path("id") String id);
}
